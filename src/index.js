// Alexa tell PhoneService to {call|ring} {James|Karolina} {*phone}  
    console.log("PhoneService v0.2 Started");

    var TWILIO_ACCOUNT_SID = 'AC3bf13efbcf5b7c79f08574233381e195';
    var TWILIO_AUTH_TOKEN = '416245d4f3c80aad3b594a5fc448f763';



    var http   = require('http');
    var twilio = require('twilio');
    var client = twilio(TWILIO_ACCOUNT_SID,TWILIO_AUTH_TOKEN);

    console.log("twilio require executed");

    var AlexaSkill = require('./AlexaSkill');
    var APP_ID = 'amzn1.echo-sdk-ams.app.799e1c35-2f6e-4e9b-9730-148f9b403ac7';

    console.log("twilio client initialized");

    var handlePhoneFinderRequest = function(intent, session, response){
      var phoneName  = intent.slots.phoneName.value;
      var cellnumber;
      var name;
      var makeCall = false;



      if(phoneName=="karolina"||phoneName=="karolinas"){
        cellnumber = '+14148611982';
        name = 'kaaroleena';
        makeCall = true;
        console.log("calling: " + phoneName);
      }else if(phoneName=="james"){
        cellnumber = '+17044923691';
        name = 'james';
        makeCall = true;
        console.log("calling: " + phoneName);
      }else{
        response.tell("unknown phone name: " + phoneName);
        console.log("unknown phoneName: " + phoneName);
      }


      if(makeCall){
        var text = 'Calling: ' + name + ' phone';

        client.calls.create({
            url: "http://wolfnodejs.azurewebsites.net/phoneservice",
            to: cellnumber, // a number to call
            from:'+17048485585', // a Twilio number you own
        }, function(err, call) {
          //NEEDS ERROR HANDLING HERE
            console.log('Call SID: '+ call.sid);

            response.tell(text);
        });
      }
  };

  var PhoneFinder = function(){
    AlexaSkill.call(this, APP_ID);
  };

  PhoneFinder.prototype = Object.create(AlexaSkill.prototype);
  PhoneFinder.prototype.constructor = PhoneFinder;

  PhoneFinder.prototype.eventHandlers.onSessionStarted = function(sessionStartedRequest, session){
    // What happens when the session starts? Optional
    console.log("onSessionStarted requestId: " + sessionStartedRequest.requestId + ", sessionId: " + session.sessionId);
  };

  PhoneFinder.prototype.eventHandlers.onLaunch = function(launchRequest, session, response){
    // This is when they launch the skill but don't specify what they want. ]
    var output = 'Welcome to Phone Finder. Say the name of the phone you want me to call';
    var reprompt = 'Which phone should I call? say call Kaaroleena or call James';

    response.ask(output, reprompt);

    console.log("onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
  };

  PhoneFinder.prototype.intentHandlers = {
    PhoneFinderIntent: function(intent, session, response){
      handlePhoneFinderRequest(intent, session, response);
    },

    HelpIntent: function(intent, session, response){
      var speechOutput = 'Get me to call your phone so that you can find it. Which phone should I call? say call Karolina or call James';
      response.ask(speechOutput);
    }
  };

  exports.handler = function(event, context) {
      var skill = new PhoneFinder();
      skill.execute(event, context);
  };
